




import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TodosService } from './../todos.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoformComponent implements OnInit {

  constructor(private TodosService:TodosService,
    private authService:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }
  title:string;
  author:string; 
  id:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add todo' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.TodosService.updateBook(this.userId, this.id,this.title,this.author);
    } else {
      console.log('In onSubmit');
      this.TodosService.addBook(this.userId,this.title)
    }
    this.router.navigate(['/todo']);  
  }  
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update book'
          this.TodosService.getBook(this.userId,this.id).subscribe(
          book => {
            console.log(book.data().author)
            console.log(book.data().title)
            this.author = book.data().author;
            this.title = book.data().title;})        
      }
   })
  }
  

}
