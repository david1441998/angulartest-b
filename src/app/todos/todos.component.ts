import { Component, OnInit } from '@angular/core';

import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { TodosService } from './../todos.service';
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
panelOpenState = false; 
todos$:Observable<any[]>;
  userId:string; 

  actionMethod(event: any) {
    event.target.disabled = true;
}

  deleteTodos(id){
    this.TodosService.deleteBook(this.userId,id)
    console.log(id);
  }
  constructor(private TodosService:TodosService,
    public authService:AuthService) { }

  ngOnInit() {
       console.log("NgOnInit started")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.todos$ = this.TodosService.getBooks(this.userId); 
  }
    )
}
}



