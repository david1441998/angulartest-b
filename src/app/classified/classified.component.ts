import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../auth.service';  
@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  userId:string;
 
  selected:string;
  constructor(public classifyService:ClassifyService,public authService:AuthService,
    private router:Router          ) {}
    text:string;
  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
       
        console.log(this.classifyService.doc)
        this.selected = this.category; 
      }
    );
    this.authService.user.subscribe(
             user => {
               this.userId = user.uid;
           })
  }
  
  onSubmit(){
    this.classifyService.addArticle(this.userId,this.selected,this.classifyService.doc)
        this.router.navigate(['/collection-classify']);
       }
}
