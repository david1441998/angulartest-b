import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';

import{RouterModule,Routes} from '@angular/router';

//angular matirel
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LayoutModule } from '@angular/cdk/layout';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';

import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';

//component
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { NavComponent } from './nav/nav.component';
import { SuccesComponent } from './succes/succes.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { CollectionClassComponent } from './collection-class/collection-class.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { UsersComponent } from './users/users.component';
import { RegusersComponent } from './regusers/regusers.component';
import { TodoformComponent } from './todoform/todoform.component';
import { TodosComponent } from './todos/todos.component';

const appRoutes: Routes = [

  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent },
  { path: 'docform', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'collection-classify', component: CollectionClassComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'rusers', component: RegusersComponent },
  { path: 'todoF', component: TodoformComponent },
  { path: 'todo', component: TodosComponent },

  { path: '',
  redirectTo: '/welcome',
  pathMatch: 'full'
},
  
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    SuccesComponent,
    DocformComponent,
    ClassifiedComponent,
    CollectionClassComponent,
    WelcomeComponent,
    UsersComponent,
    RegusersComponent,
    TodoformComponent,
    TodosComponent
  ],
  imports: [   BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatRadioModule,
    MatSidenavModule,
    MatIconModule,
    HttpClientModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    AngularFireStorageModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [ AngularFirestore, AngularFireAuth ],
  bootstrap: [AppComponent]
})
export class AppModule { }
