import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users.service';
import { User } from './../interfaces/user';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  panelOpenState = false;
  userId:string;
 
  email:string;
  password: number = 12345678;

  User$: User[];
  constructor(private UserService: UsersService, private router:Router,public auth:AuthService) { }

  ngOnInit() {
    this.UserService.getUser().subscribe(data =>this.User$ = data ); 
    this.auth.user.subscribe(
             user => {
               this.userId = user.uid;
              })
  }
  
  addbutton (id:number,email:string){ 
     this.auth.signup(email,"12345678");
    this.UserService.addUser(this.userId,email,id)
    //this.router.navigate(['/books']);
  }  
}





