import { User } from './interfaces/user';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  userCollection: AngularFirestoreCollection = this.db.collection('users');

  constructor(public afAuth:AngularFireAuth,
    private router:Router,private db: AngularFirestore) {
    this.user = this.afAuth.authState;
  }
  private logInErrorSubject = new Subject<string>();
  private signUpErrorSubject = new Subject<string>();
  getBooks(userId): Observable<any[]> {
         //const ref = this.db.collection('books');
         //return ref.valueChanges({idField: 'id'});
      
         return this.userCollection.snapshotChanges().pipe(
           map(actions => actions.map(a => {
             const data = a.payload.doc.data();
             data.id = a.payload.doc.id;
             return { ...data };
           }))
         );    
       } 

  getUser(){
        return this.user
      }
  signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res =>{
          console.log('Succesful sign up',res);
}
        ).catch (error => this.signUpErrorSubject.next(error.message))
        console.log(this.signUpErrorSubject);

  }
  logout(){
    this.afAuth.auth.signOut();
  }


 login(email:string, password:string){
   
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(res => 
      {
      console.log('Succesful Login', res);
      }).catch(error => this.logInErrorSubject.next(error.message));

  }

  public getLoginErrors(): Subject<string> {
    return this.logInErrorSubject;
  }
  public getSignUpErrors(): Subject<string> {
    return this.signUpErrorSubject;
  }


}
