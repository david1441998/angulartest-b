import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public errorMessage:string;

  constructor(public authService:AuthService,
    public router:Router) {
      this.authService.getSignUpErrors().subscribe(error=>{
        this.errorMessage=error;
      });
     }

  email: string;
  password: string;
  
  onSubmit(){
    this.authService.signup(this.email, this.password);
    //this.router.navigate(['']);
  }
  ngOnInit() {

  }

}
