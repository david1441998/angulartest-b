import { HttpClientModule, HttpClient } from '@angular/common/http';
  import { Injectable } from '@angular/core';
  import { User } from './interfaces/user';
  import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
  import { map } from 'rxjs/operators';
  import { Observable } from 'rxjs';
  import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiurl1 =  "https://jsonplaceholder.typicode.com/users"
  docCollection:AngularFirestoreCollection;
  userCollection: AngularFirestoreCollection = this.database.collection('users');

  addUser(userId: string, email: string, uid:number ) {
    const user = {email:email,uid:uid}
    this.userCollection.doc(userId).collection('userss').add(user);   }


    
  getUser(){
        return this._http.get<User[]>(this.apiurl1);
      }
      getUsers(): Observable<any[]> {
        return this.database.collection('users').valueChanges();
      } 
  constructor(private _http: HttpClient, private database: AngularFirestore,private authService:AuthService) { }
}

