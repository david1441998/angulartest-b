import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionClassComponent } from './collection-class.component';

describe('CollectionClassComponent', () => {
  let component: CollectionClassComponent;
  let fixture: ComponentFixture<CollectionClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
