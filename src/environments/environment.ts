// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAkvp7okjDagy9anhE_vPzKKXDamBGu5sE",
    authDomain: "dudu-1569681413841.firebaseapp.com",
    databaseURL: "https://dudu-1569681413841.firebaseio.com",
    projectId: "dudu-1569681413841",
    storageBucket: "dudu-1569681413841.appspot.com",
    messagingSenderId: "158819370679",
    appId: "1:158819370679:web:4d36091373e35439952f32",

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
